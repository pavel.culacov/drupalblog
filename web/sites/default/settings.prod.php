<?php

$databases['default']['default'] = array (
  'database' => 'drupal_blog',
  'username' => 'root',
  'password' => '',
  'prefix' => '',
  'host' => 'localhost',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);

$settings['trusted_host_patterns'] = [
  '^drupal-blog\.local$',
  '^.+\.drupal-blog\.local$',
];