<?php


namespace Drupal\blog_commerce\Plugin\Field\FieldFormatter;


use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_order\AdjustmentTypeManager;
use Drupal\commerce_order\PriceCalculatorInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_stock\StockServiceManager;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'commerce_product_attributes_overview'
 * formatter.
 *
 * @FieldFormatter(
 *   id = "product_price_from",
 *   label = @Translation("Product Price From"),
 *   field_types = {
 *     "entity_reference",
 *   },
 * )
 */
class ProductPriceFromFormatter extends FormatterBase implements ContainerFactoryPluginInterface{
  /**
   * The Stock Service Manager.
   *
   * @var \Drupal\commerce_stock\StockServiceManager
   */
  protected $stockServiceManager;
  /**
   * The currency formatter.
   *
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected $currencyFormatter;

  /**
   * The price calculator.
   *
   * @var \Drupal\commerce_order\PriceCalculatorInterface
   */
  protected $priceCalculator;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The current store.
   *
   * @var \Drupal\commerce_store\CurrentStoreInterface
   */
  protected $currentStore;

  /**
   * The adjustment type manager.
   *
   * @var \Drupal\commerce_order\AdjustmentTypeManager
   */
  protected $adjustmentTypeManager;

  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition,
                              array $settings, $label, $view_mode, array $third_party_settings,
                              StockServiceManager $simple_stock_manager, CurrencyFormatterInterface $currency_formatter,
                              PriceCalculatorInterface $price_calculator,
                              CurrentStoreInterface $current_store, AccountInterface $current_user,
                              AdjustmentTypeManager $adjustment_type_manager
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->stockServiceManager = $simple_stock_manager;
    $this->currencyFormatter = $currency_formatter;
    $this->priceCalculator = $price_calculator;
    $this->currentStore = $current_store;
    $this->currentUser = $current_user;
    $this->adjustmentTypeManager = $adjustment_type_manager;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('commerce_stock.service_manager'),
      $container->get('commerce_price.currency_formatter'),
      $container->get('commerce_order.price_calculator'),
      $container->get('commerce_store.current_store'),
      $container->get('current_user'),
      $container->get('plugin.manager.commerce_adjustment_type')
    );
  }
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'adjustment_types' => [],
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['adjustment_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Adjustments'),
      '#options' => [],
      '#default_value' => $this->getSetting('adjustment_types'),
    ];
    foreach ($this->adjustmentTypeManager->getDefinitions() as $plugin_id => $definition) {
      if (!in_array($plugin_id, ['custom'])) {
        $label = $this->t('Apply @label to the calculated price', ['@label' => $definition['plural_label']]);
        $elements['adjustment_types']['#options'][$plugin_id] = $label;
      }
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $enabled_adjustment_types = array_filter($this->getSetting('adjustment_types'));
    foreach ($this->adjustmentTypeManager->getDefinitions() as $plugin_id => $definition) {
      if (in_array($plugin_id, $enabled_adjustment_types)) {
        $summary[] = $this->t('Apply @label to the calculated price', ['@label' => $definition['plural_label']]);
      }
    }

    return $summary;
  }

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemList $fieldItemListReference */
    $fieldItemListReference = $items;
    $variationPrice = null;
    /**
     * @var  $delta
     * @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation
     */
    foreach ($fieldItemListReference->referencedEntities() as $delta => $variation){
      //2) Variation is Active
      if(!$variation->isPublished()){
        continue;
      }
      //4) Stock > 0
      if(!$this->isInStock($variation)){
        continue;
      }
      if(is_null($variationPrice)){
        $variationPrice = $variation;
        continue;
      }
      if($variation->getPrice()->lessThan($variationPrice->getPrice())){
        $variationPrice = $variation;
      }
    }

    //1)No Variation
    if(is_null($variationPrice)){
      return $elements;
    }
    //5) Discount system
    $context = new Context($this->currentUser, $this->currentStore->getStore(), NULL, [
      'price' => $items->getName(),
    ]);
    $adjustment_types = array_filter($this->getSetting('adjustment_types'));
    $result = $this->priceCalculator->calculate($variationPrice, 1, $context, $adjustment_types);
    $price = $result->getCalculatedPrice();
    //3) Formatter price
    $elements[] = [
      '#markup' => "At: ".$this->currencyFormatter->format($price->getNumber(), $price->getCurrencyCode(), [
        'minimum_fraction_digits' => 2,
        'maximum_fraction_digits' => 2,
        'currency_display' => 'symbol'
      ])
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $entity_type = $field_definition->getTargetEntityTypeId();
    $field_name = $field_definition->getName();
    return $entity_type == 'commerce_product' && $field_name == 'variations';
  }
  protected function isInStock(ProductVariationInterface  $variation){

    $alwaysStock = true;
    if($variation->hasField('commerce_stock_always_in_stock')){
      $alwaysStock = $variation->get('commerce_stock_always_in_stock')->value == 1;
    }
    if ($variation instanceof PurchasableEntityInterface) {
      // Get the available Stock for the product variation.
      $stockServiceManager = $this->stockServiceManager;
      $level = $stockServiceManager->getStockLevel($variation);
    }
    else {
      // No stock if this is not a purchasable entity.
      $level = 0;
    }
    return  $level > 0 || $alwaysStock;
  }
}
