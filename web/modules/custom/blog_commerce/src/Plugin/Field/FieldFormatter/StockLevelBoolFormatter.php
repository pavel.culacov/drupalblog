<?php


namespace Drupal\blog_commerce\Plugin\Field\FieldFormatter;


use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_stock_field\Plugin\Field\FieldFormatter\SimpleStockLevelFormatter;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;

/**
 *
 *
 * @FieldFormatter(
 *   id = "commerce_stock_level_bool",
 *   module = "blog_commerce",
 *   label = @Translation("Simple stock level Bool"),
 *   field_types = {
 *     "commerce_stock_level"
 *   }
 * )
 */
class StockLevelBoolFormatter extends SimpleStockLevelFormatter {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    // Get the entity.
    $entity = $items->getEntity();
    $alwaysStock = true;
    if($entity->hasField('commerce_stock_always_in_stock')){
      $alwaysStock = $entity->get('commerce_stock_always_in_stock')->value == 1;
    }
    if ($entity instanceof PurchasableEntityInterface) {
      // Get the available Stock for the product variation.
      $stockServiceManager = $this->stockServiceManager;
      $level = $stockServiceManager->getStockLevel($entity);
    }
    else {
      // No stock if this is not a purchasable entity.
      $level = 0;
    }
    $elements = [];
    // Return a single item.
    $elements[0] = [
      '#markup' => $level > 0 || $alwaysStock ? $this->t("Yes") : $this->t("No")
    ];

    return $elements;
  }
}
