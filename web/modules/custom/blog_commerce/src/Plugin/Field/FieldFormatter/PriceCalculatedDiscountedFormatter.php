<?php
namespace Drupal\blog_commerce\Plugin\Field\FieldFormatter;

use Drupal\commerce\Context;
use Drupal\commerce_order\Plugin\Field\FieldFormatter\PriceCalculatedFormatter;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Plugin implementation of the 'commerce_price_calculated' formatter.
 *
 * @FieldFormatter(
 *   id = "commerce_price_calculated_discounted",
 *   label = @Translation("Calculated discounted"),
 *   field_types = {
 *     "commerce_price"
 *   }
 * )
 */
class PriceCalculatedDiscountedFormatter extends PriceCalculatedFormatter {

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    if (!$items->isEmpty()) {
      $context = new Context($this->currentUser, $this->currentStore->getStore(), NULL, [
        'field_name' => $items->getName(),
      ]);
      /** @var \Drupal\commerce\PurchasableEntityInterface $purchasable_entity */
      $purchasable_entity = $items->getEntity();
      $adjustment_types = array_filter($this->getSetting('adjustment_types'));
      $result = $this->priceCalculator->calculate($purchasable_entity, 1, $context, $adjustment_types);
      //
      $calculated_price = $result->getCalculatedPrice();
      $number = $calculated_price->getNumber();
      $currency_code = $calculated_price->getCurrencyCode();
      $options = $this->getFormattingOptions();
      $price = $purchasable_entity->getPrice();
      $discountedPriceFormatter = $this->currencyFormatter->format($number, $currency_code, $options);
      $originalPriceFormatter = $this->currencyFormatter->format($price->getNumber(), $price->getCurrencyCode(), $options);
      $markup = sprintf("%s, <del>%s</del>", $discountedPriceFormatter, $originalPriceFormatter);
      $elements[0] = [
        '#markup' => $markup,
        '#cache' => [
          'tags' => $purchasable_entity->getCacheTags(),
          'contexts' => Cache::mergeContexts($purchasable_entity->getCacheContexts(), [
            'languages:' . LanguageInterface::TYPE_INTERFACE,
            'country',
          ]),
        ],
      ];
    }

    return $elements;
  }

}
