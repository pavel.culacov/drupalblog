<?php

namespace Drupal\blog_commerce\Plugin\Block;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\Plugin\Block\CartBlock;
use Drupal\commerce_price\Price;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CustomCartBlock
 *
 * @package Drupal\blog_commerce\Plugin\Block
 * @Block(
 *   id="custom_commerce_cart_block",
 *   admin_label=@Translation("Cart(Custom)"),
 *   category="Utilvideo"
 * )
 */
class CustomCartBlock extends CartBlock {
  /**
   * The currency formatter.
   *
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected $currencyFormatter;
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CartProviderInterface $cart_provider, EntityTypeManagerInterface $entity_type_manager, CurrencyFormatterInterface $currency_formatter) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $cart_provider, $entity_type_manager);
    $this->currencyFormatter = $currency_formatter;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_cart.cart_provider'),
      $container->get('entity_type.manager'),
      $container->get('commerce_price.currency_formatter')
    );
  }

  public function build() {
    $cachable_metadata = new CacheableMetadata();
    $cachable_metadata->addCacheContexts(['user', 'session']);

    /** @var \Drupal\commerce_order\Entity\OrderInterface[] $carts */
    $carts = $this->cartProvider->getCarts();//Order Type
    $carts = array_filter($carts, function ($cart) {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $cart */
      // There is a chance the cart may have converted from a draft order, but
      // is still in session. Such as just completing check out. So we verify
      // that the cart is still a cart.
      return $cart->hasItems() && $cart->cart->value;
    });

    $count = 0;
    $total = new Price(0, "USD");
    if (!empty($carts)) {
      foreach ($carts as $cart_id => $cart) {
        $total = $total->add($cart->getTotalPrice());// NEW PRICE
        foreach ($cart->getItems() as $order_item) {//Order Item Type
          $count += (int) $order_item->getQuantity();
        }
        $cachable_metadata->addCacheableDependency($cart);
      }
    }

    $options = [
      'currency_display' => "code",//none, symbol, code
      'minimum_fraction_digits' => 0,
      'maximum_fraction_digits' => 2
    ];

    return [
      '#theme' => 'custom_cart',
      '#count' => $count,
      '#total' => $this->currencyFormatter->format($total->getNumber(), $total->getCurrencyCode(), $options),
      '#count_text' => $this->formatPlural($count, '@count item', '@count items'),
      '#url' => Url::fromRoute('commerce_cart.page')->toString(),
      '#cache' => [
        'contexts' => ['cart'],
      ],
    ];

  }


}
