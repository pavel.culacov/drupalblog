<?php

namespace Drupal\blog_commerce;

use Drupal\block\BlockRepositoryInterface;
use Drupal\block\Entity\Block;
use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\OrderTotalSummaryInterface;
use Drupal\Core\Ajax\AfterCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\RendererInterface;

class AjaxCardHandler {

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * @var \Drupal\block\BlockRepositoryInterface
   */
  protected $blockRepository;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\commerce_order\OrderTotalSummaryInterface
   */
  protected $orderTotalSummary;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $orderStorage;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $orderItemStorage;

  /**
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;

  public function __construct(
    MessengerInterface $messenger, RendererInterface $renderer,
    BlockRepositoryInterface $blockRepository, EntityTypeManagerInterface $entityTypeManager,
    OrderTotalSummaryInterface $orderTotalSummary, CartManagerInterface  $cartManager) {
    $this->messenger = $messenger;
    $this->renderer = $renderer;
    $this->blockRepository = $blockRepository;
    $this->entityTypeManager = $entityTypeManager;
    $this->orderTotalSummary = $orderTotalSummary;
    $this->orderStorage = $entityTypeManager->getStorage('commerce_order');
    $this->orderItemStorage = $entityTypeManager->getStorage('commerce_order_item');
    $this->cartManager = $cartManager;
  }

  /**
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  protected function ajaxDefault() {
    $ajax = new AjaxResponse();
    $ajax->addCommand(new RemoveCommand('div[data-drupal-messages]'));
    $ajax->addCommand(new AfterCommand('div[data-drupal-messages-fallback]', $this->getMessenger()));
    $ajax->addCommand(new ReplaceCommand('.block-custom-commerce-cart-block', $this->getCartBlock()));
    return $ajax;
  }

  public function addProduct($form, FormStateInterface $formState) {
    return $this->ajaxDefault();
  }

  public function removeOrderItem($form, FormStateInterface $form_state) {
    $ajax = $this->ajaxDefault();
    $triggering_element = $form_state->getTriggeringElement();
    if ($triggering_element['#remove_order_item'] == TRUE && !empty($triggering_element['#order_id'])) {
      $orderId = (int) $triggering_element['#order_id'];
      $orderItemId = (int) $triggering_element['#order_item_id'];
      /** @var OrderInterface $order */
      $order = $this->orderStorage->load($orderId);
      $ajax->addCommand(new HtmlCommand('div[data-drupal-selector="order-total-summary"]', $this->getCartSummary($order)));
      $ajax->addCommand(new RemoveCommand('#order_item_row_' . $orderItemId));//$('#order_item_row_1').remove();
    }
    return $ajax;
  }

  public function updateOrderItem($form, FormStateInterface $form_state) {
    $ajax = $this->ajaxDefault();
    $triggering_element = $form_state->getTriggeringElement();
    if (!empty($triggering_element['#order_item_id'])) {
      $value = (int) $triggering_element['#value'];
      //      if($value < 1 || $value > 9999)
      //        return;
      $orderItemId = (int) $triggering_element['#order_item_id'];
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $orderItem */
      $orderItem = $this->orderItemStorage->load($orderItemId);
      if ($orderItem->getQuantity() == $value) {
        return $ajax;
      }
      $orderItem->setQuantity($value);
      $order = $orderItem->getOrder();
      $this->cartManager->updateOrderItem($order, $orderItem);
      $ajax->addCommand(new HtmlCommand('div[data-drupal-selector="order-total-summary"]', $this->getCartSummary($order)));
      $totalHtmlId = sprintf('#order_item_row_%s .views-field-total-price__number .field-content',$orderItem->id() );
      $ajax->addCommand(new HtmlCommand($totalHtmlId, $orderItem->getTotalPrice()->getNumber()));
    }
    return $ajax;
  }

  protected function getMessenger() {
    $message = [
      '#theme' => 'status_messages',
      '#message_list' => $this->messenger->all(),
    ];
    try {
      $messages = $this->renderer->render($message);
    } catch (\Exception $e) {
    }
    $this->messenger->deleteAll();
    return $messages;
  }

  /**
   * Gets cart block.
   *
   * @return array
   *   Return render array.
   */
  private function getCartBlock() {
    $block_id = $this->getCartBlockId();
    if ($block_id !== FALSE) {
      $block = Block::load($block_id);
      $render = $this->entityTypeManager
        ->getViewBuilder('block')
        ->view($block);
    }
    return isset($render) ? $render : NULL;
  }

  /**
   * Gets the machine name of a commerce cart block visible on the current page.
   *
   * Returns only the first cart found.
   *
   * @return string|false
   *   Return id of the first commerce cart block found on current page.
   *   Returns FALSE if no commerce cart block is visible.
   */
  private function getCartBlockId() {
    // Returns an array of regions each with an array of blocks.
    $regions = $this->blockRepository->getVisibleBlocksPerRegion();

    // Iterate all visible blocks and regions.
    foreach ($regions as $region) {
      foreach ($region as $block) {
        $plugin_id = $block->get('plugin');
        // Check if this is a commerce cart block.
        if ($plugin_id === 'custom_commerce_cart_block') {
          $cart_block_id = $block->id();
          return $cart_block_id;
        }
      }
    }

    return FALSE;
  }

  private function getCartSummary(OrderInterface $order) {
    $cartSummary = [
      '#theme' => 'commerce_order_total_summary',
      '#order_entity' => $order,
      '#totals' => $this->orderTotalSummary->buildTotals($order),
    ];
    return $this->renderer->render($cartSummary);
  }

}
