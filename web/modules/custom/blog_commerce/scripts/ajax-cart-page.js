'use strict';
(function ($, _, drupalSettings) {

  Drupal.behaviors.ajax_cart_page = {
    attach: function (context, settings) {
      var cartRow = $('.view-commerce-cart-form .views-row');
      if(cartRow.length < 1){
         location.reload()
      }
    }
  };
})(jQuery, _, drupalSettings);
