<?php

namespace Drupal\blog_core\Plugin\Paragraphs\Behavior;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Annotation\ParagraphsBehavior;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;

/**
 * Class ParagraphImageBehavior
 *
 * @package Drupal\blog_core\Plugin\Paragraph\Behavior
 * @ParagraphsBehavior(
 *   id="paragraph_gallery",
 *   label = "Gallery Settings",
 *   description="Gallery Description",
 *   weight=10
 * )
 */
class ParagraphImageBehavior extends ParagraphsBehaviorBase {

  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
    $column = $this->getColumnValue($paragraph);
    $style = $this->getStyleValue($paragraph);
    if($style != "none"){
      $fieldGallery = $paragraph->get("field_gallery");
      foreach ($fieldGallery as $k => $item){
        $build["field_gallery"][$k]["#display_settings"]["photoswipe_node_style"] = $style;
      }

    }
    if($column != "none"){
      $build['#attributes']['class'][] = Html::getClass($column);
    }
    $build['#attached']["library"][] = "blog_core/paragraph_gallery_behavior";
  }

  public function settingsSummary(Paragraph $paragraph) {
    $column = $this->getColumnValue($paragraph);
    $style = $this->getStyleValue($paragraph);

    $summary = [];
    if($column != "none"){
      $summary[] = "Column: ".$this->getColumns()[$column];
    }
    if($style != "none"){
      $summary[] = "Style: ".$this->getStyles()[$style];
    }
    return $summary;
  }

  public function buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    parent::buildBehaviorForm($paragraph, $form, $form_state); // TODO: Change the autogenerated stub
    $form["column"] = [
      "#type" => "select",
      "#title" => "Columns",
      "#options" => $this->getColumns(),
      "#default_value" => $this->getColumnValue($paragraph)
    ];
    $form["style"] = [
      "#type" => "select",
      "#title" => "Style",
      "#options" => $this->getStyles(),
      "#default_value" => $this->getStyleValue($paragraph)
    ];

  }
  protected function getStyles(){
    return [
      "none" => "None",
      "500x500" => "500x500"
    ];
  }
  protected function getColumns(){
    return [
      "none" => "None",
      "column_2" => "Column 2",
      "column_3" => "Column 3",
    ];
  }
  protected function getColumnValue(ParagraphInterface $paragraph){
    return $paragraph->getBehaviorSetting($this->getPluginId(), "column", "none");
  }
  protected function getStyleValue(ParagraphInterface $paragraph){
    return $paragraph->getBehaviorSetting($this->getPluginId(), "style", "none");
  }

  public static function isApplicable(ParagraphsType $paragraphs_type) {
    return $paragraphs_type->id() == "gallery";
  }


}
