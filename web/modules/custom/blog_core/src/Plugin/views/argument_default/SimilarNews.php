<?php

namespace Drupal\blog_core\Plugin\views\argument_default;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;
use Drupal\views\Annotation\ViewsArgumentDefault;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Taxonomy SimilarNews default argument.
 *
 * @ViewsArgumentDefault(
 *   id = "similar_new",
 *   title = @Translation("Similar News")
 * )
 */
class SimilarNews extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entityTypeManager;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('entity_type.manager')
    );
  }


  public function getArgument() {
    if (($node = $this->routeMatch->getParameter('node')) && $node instanceof NodeInterface && $node->hasField('field_category_news')) {
      $fieldCategory = $node->get('field_category_news');
      $tids = [];
      /**
       * @var \Drupal\taxonomy\TermInterface $term
       */
      foreach ($fieldCategory->referencedEntities() as $term) {
        $termSimilarNewField = $term->get('field_similar_news');
        if($termSimilarNewField->isEmpty()){
          continue;
        }
        $tids = array_merge($tids, [$termSimilarNewField->getString()]);
      }
      return !empty($tids) ? implode(",", $tids) : $fieldCategory->getString();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['url'];
  }

}
